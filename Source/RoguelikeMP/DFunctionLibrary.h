#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DFunctionLibrary.generated.h"

UCLASS()
class ROGUELIKEMP_API UDFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	static FString GetAddress(UObject* WorldContext);

	UFUNCTION(BlueprintCallable)
	static void ConnectToServer(FString URL, APlayerController* PC);
	
};
