#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "GameFramework/PlayerStart.h"
#include "RoguelikeMP.h"
#include "Materials/Material.h"
#include "DWorldGen.generated.h"

USTRUCT(BlueprintType)
struct FWorldGenDat
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere)
	TArray<FVector> Vertices;
	UPROPERTY(VisibleAnywhere)
	TArray<int32> Indices;
	UPROPERTY(VisibleAnywhere)
	TArray<FVector> Normals;
	UPROPERTY(VisibleAnywhere)
	TArray<FVector2D> TexCoords;
	UPROPERTY(VisibleAnywhere)
	TArray<FProcMeshTangent> Tangents;
	UPROPERTY(VisibleAnywhere)
	TArray<FLinearColor> Colors;
};

UCLASS(BlueprintType)
class ROGUELIKEMP_API UPartitionNode : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere)
	float XPos;
	UPROPERTY(VisibleAnywhere)
	float YPos;
	UPROPERTY(VisibleAnywhere)
	float Width;
	UPROPERTY(VisibleAnywhere)
	float Height;
	UPROPERTY(VisibleAnywhere)
	int32 RoomX;
	UPROPERTY(VisibleAnywhere)
	int32 RoomY;
	UPROPERTY(VisibleAnywhere)
	int32 RoomWidth;
	UPROPERTY(VisibleAnywhere)
	int32 RoomHeight;
	UPROPERTY(VisibleAnywhere)
	UPartitionNode* Left;
	UPROPERTY(VisibleAnywhere)
	UPartitionNode* Right;

	FVector2D RoomMidpoint()
	{
		return FVector2D(RoomX + Width / 2, RoomY + Height / 2);
	}

	bool IsLeaf() 
	{
		if (Left == nullptr && Right == nullptr)
			return true;
		else
			return false;
	};

};

USTRUCT()
struct FPartitionPair
{
	GENERATED_BODY()

	UPartitionNode* First;
	UPartitionNode* Second;
	float Distance;
};

UCLASS()
class ROGUELIKEMP_API ADWorldGen : public AActor
{
	GENERATED_BODY()
	
public:	
	ADWorldGen();

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* WorldMesh;
	bool** IsRoom;

	UPROPERTY(EditAnywhere)
	float TileSize;
	UPROPERTY(EditAnywhere)
	float RoomHeight;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 GridWidth;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 GridHeight;

	UPROPERTY(EditAnywhere)
	float MinPartition;

	UPROPERTY(EditAnywhere)
	UMaterial* WallMaterial;
	UPROPERTY(EditAnywhere)
	UMaterial* FloorMaterial;
	UPROPERTY(EditAnywhere)
	UMaterial* CeilingMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Seed;

	virtual void Tick(float DeltaTime) override;

	virtual void Destroyed() override;

	UFUNCTION(BlueprintCallable)
	bool IsRoomAt(int32 TileX, int32 TileY);

protected:
	virtual void BeginPlay() override;

private:


	int32 FloorVerts;
	int32 WallVerts;
	int32 CeilVerts;

	UPartitionNode* CreatePartitionNode(float XPos, float YPos, float Width, float Height);
	void PartitionGrid(UPartitionNode* Parent);
	void PartitionGrid_Horizontal(UPartitionNode* Parent);
	void PartitionGrid_Vertical(UPartitionNode* Parent);
	void GenRooms(UPartitionNode* RootPartition);
	void GenHalls(UPartitionNode* RootPartition);
	void GenMesh(FWorldGenDat& FloorWorldDat, FWorldGenDat& WallWorldDat, FWorldGenDat& CeilWorldDat);
	void FinishGen(FWorldGenDat& FloorWorldDat, FWorldGenDat& WallWorldDat, FWorldGenDat& CeilWorldDat);
	FPartitionPair MinDistance_LeafLeaf(UPartitionNode* LeafOne, UPartitionNode* LeafTwo);
	FPartitionPair MinDistance_LeafParent(UPartitionNode* LeafOne, UPartitionNode* LeafTwo);
	FPartitionPair MinDistance_ParentParent(UPartitionNode* LeafOne, UPartitionNode* LeafTwo);
	bool DoesIntersectY(UPartitionNode* One, UPartitionNode* Two);
	bool DoesIntersectX(UPartitionNode* One, UPartitionNode* Two);
	int32 IntersectionYStart(UPartitionNode* One, UPartitionNode* Two);
	int32 IntersectionYEnd(UPartitionNode* One, UPartitionNode* Two);
	int32 IntersectionXStart(UPartitionNode* One, UPartitionNode* Two);
	int32 IntersectionXEnd(UPartitionNode* One, UPartitionNode* Two);

	bool HasWall(int32 TileX, int32 TileY, int32 SideX, int32 SideY);

	void GenFloor(int32 TileX, int32 TileY, FWorldGenDat& GenDat);
	void GenWall_Left(int32 TileX, int32 TileY, FWorldGenDat& GenDat);
	void GenWall_Right(int32 TileX, int32 TileY, FWorldGenDat& GenDat);
	void GenWall_Front(int32 TileX, int32 TileY, FWorldGenDat& GenDat);
	void GenWall_Back(int32 TileX, int32 TileY, FWorldGenDat& GenDat);
	void GenCeiling(int32 TileX, int32 TileY, FWorldGenDat& GenDat);

	FVector TileVertex_Bottom_BottomLeft(int32 TileX, int32 TileY);
	FVector TileVertex_Bottom_BottomRight(int32 TileX, int32 TileY);
	FVector TileVertex_Bottom_TopLeft(int32 TileX, int32 TileY);
	FVector TileVertex_Bottom_TopRight(int32 TileX, int32 TileY);
	FVector TileVertex_Top_BottomLeft(int32 TileX, int32 TileY);
	FVector TileVertex_Top_BottomRight(int32 TileX, int32 TileY);
	FVector TileVertex_Top_TopLeft(int32 TileX, int32 TileY);
	FVector TileVertex_Top_TopRight(int32 TileX, int32 TileY);

};
