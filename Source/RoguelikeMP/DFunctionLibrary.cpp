// Fill out your copyright notice in the Description page of Project Settings.

#include "DFunctionLibrary.h"
#include "Engine/World.h"
#include "Kismet/KismetStringLibrary.h"
#include "GameFramework/PlayerController.h"

FString UDFunctionLibrary::GetAddress(UObject* WorldContext)
{
	if (WorldContext)
	{
		UWorld* World = WorldContext->GetWorld();
		FString Result = TEXT("");

		Result = Result.Append(World->URL.Host);
		Result = Result.Append(":");
		Result = Result.Append(UKismetStringLibrary::Conv_IntToString(World->URL.Port));

		return Result;
	}

	return TEXT("");

}

void UDFunctionLibrary::ConnectToServer(FString URL, APlayerController* PC)
{
	PC->ClientTravel(URL, ETravelType::TRAVEL_Absolute);
}
