#include "DWorldGen.h"

ADWorldGen::ADWorldGen()
{
	PrimaryActorTick.bCanEverTick = true;

	WorldMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("WorldMesh"));

	this->TileSize = 1000.0f;
	this->RoomHeight = 500.0f;
	this->GridWidth = 20;
	this->GridHeight = 20;
	this->MinPartition = 4;
}

void ADWorldGen::BeginPlay()
{
	Super::BeginPlay();

	FWorldGenDat FloorGenDat;
	FWorldGenDat CeilGenDat;
	FWorldGenDat WallGenDat;

	UPartitionNode* Root;

	// Initialize FRand seed.
	// TODO: make this seed based on time or custom input.
	FMath::SRandInit(Seed);
	
	// Set initial world generator state.
	WallVerts = 0;
	CeilVerts = 0;

	IsRoom = new bool*[GridHeight];
	for (int32 Row = 0; Row < GridHeight; Row++)
	{
		IsRoom[Row] = new bool[GridWidth];
		for (int32 Col = 0; Col < GridWidth; Col++)
		{
			IsRoom[Row][Col] = false;
		}
	}

	// Partition the world.
	Root = CreatePartitionNode(0, 0, GridWidth, GridHeight);
	PartitionGrid(Root);

	// Generate rooms in the world.
	GenRooms(Root);

	// Generate hallways
	GenHalls(Root);

	// Generate mesh based on generated floor plan.
	GenMesh(FloorGenDat, WallGenDat, CeilGenDat);

	// Finish generating the mesh based on the floor plan.
	FinishGen(FloorGenDat, WallGenDat, CeilGenDat);

	// Determine where to start the player (add APlayerStart).	
}

void ADWorldGen::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADWorldGen::Destroyed()
{
}

UPartitionNode* ADWorldGen::CreatePartitionNode(float XPos, float YPos, float Width, float Height)
{
	UPartitionNode* NewNode = NewObject<UPartitionNode>();
	NewNode->XPos = XPos;
	NewNode->YPos = YPos;
	NewNode->Width = Width;
	NewNode->Height = Height;

	NewNode->Left = nullptr;
	NewNode->Right = nullptr;

	return NewNode;
}

void ADWorldGen::PartitionGrid(UPartitionNode* Parent)
{
	// Pick horizontal or vertical split.

	bool bCanVertical = true;
	bool bCanHorizontal = true;

	if (Parent->Width < 2 * MinPartition)
		bCanVertical = false;
	if (Parent->Height < 2 * MinPartition)
		bCanHorizontal = false;
	
	if (bCanVertical && !bCanHorizontal)
	{
		PartitionGrid_Vertical(Parent);
	}
	else if (!bCanVertical && bCanHorizontal)
	{
		PartitionGrid_Horizontal(Parent);
	}
	else if (bCanVertical && bCanHorizontal)
	{
		if (FMath::SRand() > 0.5f)
		{
			PartitionGrid_Vertical(Parent);
		}
		else
		{
			PartitionGrid_Horizontal(Parent);
		}
	}
	else
	{
		// Don't split anymore, we're done partitioning.

	}
}

void ADWorldGen::PartitionGrid_Horizontal(UPartitionNode* Parent)
{
	float MinPartitionValue = MinPartition;
	float MaxPartitionValue = Parent->Height - MinPartition;
	float PartitionRange = MaxPartitionValue - MinPartitionValue;
	float PartitionValue = FMath::SRand() * PartitionRange + MinPartitionValue;

	// Bottom
	Parent->Left = CreatePartitionNode(Parent->XPos, Parent->YPos, Parent->Width, PartitionValue);
	// Top
	Parent->Right = CreatePartitionNode(Parent->XPos, Parent->YPos + PartitionValue, Parent->Width, Parent->Height - PartitionValue);

	// Recursively partition the children
	PartitionGrid(Parent->Left);
	PartitionGrid(Parent->Right);

}

void ADWorldGen::PartitionGrid_Vertical(UPartitionNode* Parent)
{
	float MinPartitionValue = MinPartition;
	float MaxPartitionValue = Parent->Width - MinPartition;
	float PartitionRange = MaxPartitionValue - MinPartitionValue;
	float PartitionValue = FMath::SRand() * PartitionRange + MinPartitionValue;

	Parent->Left = CreatePartitionNode(Parent->XPos, Parent->YPos, PartitionValue, Parent->Height);
	Parent->Right = CreatePartitionNode(Parent->XPos + PartitionValue, Parent->YPos, Parent->Width - PartitionValue, Parent->Height);

	// Recursively partition the children
	PartitionGrid(Parent->Left);
	PartitionGrid(Parent->Right);
}

void ADWorldGen::GenRooms(UPartitionNode* RootPartition)
{
	// Generate the rooms from [1,n-1] on both dimensions in each world.

	if (RootPartition->IsLeaf())
	{
		RootPartition->RoomX = FMath::FloorToInt(RootPartition->XPos);
		RootPartition->RoomY = FMath::FloorToInt(RootPartition->YPos);
		RootPartition->RoomWidth = FMath::FloorToInt(RootPartition->Width) - 1;
		RootPartition->RoomHeight = FMath::FloorToInt(RootPartition->Height) - 1;
		int32 EndX = RootPartition->RoomX + RootPartition->RoomWidth - 1;
		int32 EndY = RootPartition->RoomY + RootPartition->RoomHeight - 1;

		int32 TileX, TileY;

		for (TileX = RootPartition->RoomX; TileX <= EndX; TileX++)
		{
			for (TileY = RootPartition->RoomY; TileY <= EndY; TileY++)
			{
				IsRoom[TileY][TileX] = true;
			}
		}

	}
	else
	{
		// Recurse until we reach leaves
		GenRooms(RootPartition->Left);
		GenRooms(RootPartition->Right);
	}
}

void ADWorldGen::GenHalls(UPartitionNode* RootPartition)
{
	// If we're a leaf we're done generating halls.
	if (RootPartition->IsLeaf())
		return;

	bool bLeftLeaf = RootPartition->Left->IsLeaf();
	bool bRightLeaf = RootPartition->Right->IsLeaf();
	FPartitionPair Pair;

	if (bLeftLeaf && bRightLeaf)
	{
		Pair = MinDistance_LeafLeaf(RootPartition->Left, RootPartition->Right);
	}
	else if (bLeftLeaf && !bRightLeaf)
	{
		Pair = MinDistance_LeafParent(RootPartition->Left, RootPartition->Right);
	}
	else if (!bLeftLeaf && bRightLeaf)
	{
		Pair = MinDistance_LeafParent(RootPartition->Right, RootPartition->Left);
	}
	else
	{
		Pair = MinDistance_ParentParent(RootPartition->Left, RootPartition->Right);
	}

	// Gen hall between the closest halls midpoint

	// Vertical split (connect vertically)
	if (DoesIntersectX(Pair.First, Pair.Second))
	{
		int32 XStart = IntersectionXStart(Pair.First, Pair.Second);
		int32 XEnd = IntersectionXEnd(Pair.First,Pair.Second);
		int32 MidX = (XStart + XEnd) / 2;
		int32 YStart, YEnd;

		if (Pair.First->RoomY > Pair.Second->RoomY)
		{
			YStart = Pair.Second->RoomY + Pair.Second->RoomHeight;
			YEnd = Pair.First->RoomY;
		}
		else
		{
			YStart = Pair.First->RoomY + Pair.First->RoomHeight;
			YEnd = Pair.Second->RoomY;
		}

		for (int32 Y = YStart; Y <= YEnd; Y++)
			IsRoom[Y][MidX] = true;
	}
	else if(DoesIntersectY(Pair.First, Pair.Second))
	{
		int32 YStart = IntersectionYStart(Pair.First, Pair.Second);
		int32 YEnd = IntersectionYEnd(Pair.First, Pair.Second);
		int32 MidY = (YStart + YEnd) / 2;
		int32 XStart, XEnd;

		if (Pair.First->RoomX > Pair.Second->RoomX)
		{
			XStart = Pair.Second->RoomX + Pair.Second->RoomWidth;
			XEnd = Pair.First->RoomX;
		}
		else
		{
			XStart = Pair.First->RoomX + Pair.First->RoomWidth;
			XEnd = Pair.Second->RoomX;
		}

		for (int32 X = XStart; X <= XEnd; X++)
			IsRoom[MidY][X] = true;
	}

	// Recursively generate halls for left and right children.
	GenHalls(RootPartition->Left);
	GenHalls(RootPartition->Right);
}

bool ADWorldGen::DoesIntersectY(UPartitionNode* One, UPartitionNode* Two)
{
	if (One->RoomY >= Two->RoomY && One->RoomY < Two->RoomY + Two->RoomHeight)
		return true;
	if (One->RoomY + One->RoomHeight >= Two->RoomY && One->RoomY + One->RoomHeight < Two->RoomY + Two->RoomHeight)
		return true;
	if (Two->RoomY >= One->RoomY && Two->RoomY < One->RoomY + One->RoomHeight)
		return true;
	if (Two->RoomY + Two->RoomHeight >= One->RoomY && Two->RoomY + Two->RoomHeight < One->RoomY + One->RoomHeight)
		return true;

	return false;
}

bool ADWorldGen::DoesIntersectX(UPartitionNode* One, UPartitionNode* Two)
{
	if (One->RoomX >= Two->RoomX && One->RoomX < Two->RoomX + Two->RoomWidth)
		return true;
	if (One->RoomX + One->RoomWidth >= Two->RoomX && One->RoomX + One->RoomWidth < Two->RoomX + Two->RoomWidth)
		return true;
	if (Two->RoomX >= One->RoomX && Two->RoomX < One->RoomX + One->RoomWidth)
		return true;
	if (Two->RoomX + Two->RoomWidth >= One->RoomX && Two->RoomX + Two->RoomWidth < One->RoomX + One->RoomWidth)
		return true;

	return false;
}

int32 ADWorldGen::IntersectionYStart(UPartitionNode* One, UPartitionNode* Two)
{
	if (One->RoomY > Two->RoomY)
		return One->RoomY;
	else
		return Two->RoomY;
}

int32 ADWorldGen::IntersectionYEnd(UPartitionNode* One, UPartitionNode* Two)
{
	if (One->RoomY > Two->RoomY)
		return Two->RoomY + Two->RoomHeight;
	else
		return One->RoomY + One->RoomHeight;
}
int32 ADWorldGen::IntersectionXStart(UPartitionNode* One, UPartitionNode* Two)
{
	if (One->RoomX > Two->RoomX)
		return One->RoomX;
	else
		return Two->RoomX;
}

int32 ADWorldGen::IntersectionXEnd(UPartitionNode* One, UPartitionNode* Two)
{
	if (One->RoomX > Two->RoomX)
		return Two->RoomX + Two->RoomWidth;
	else
		return One->RoomX + One->RoomWidth;
}

FPartitionPair ADWorldGen::MinDistance_LeafLeaf(UPartitionNode* LeafOne, UPartitionNode* LeafTwo)
{
	FVector2D MidpointOne = LeafOne->RoomMidpoint();
	FVector2D MidpointTwo = LeafTwo->RoomMidpoint();
	FPartitionPair Result;
	Result.First = LeafOne;
	Result.Second = LeafTwo;
	Result.Distance = FVector2D::Distance(MidpointOne, MidpointTwo);;

	return Result;
}

FPartitionPair ADWorldGen::MinDistance_LeafParent(UPartitionNode* LeafOne, UPartitionNode* Parent)
{
	FPartitionPair LeftPair, RightPair;
	LeftPair.Distance = -1;
	RightPair.Distance = -1;

	if (Parent->Left)
	{
		if (Parent->Left->IsLeaf())
			LeftPair = MinDistance_LeafLeaf(LeafOne, Parent->Left);
		else
			LeftPair = MinDistance_LeafParent(LeafOne, Parent->Left);
	}

	if (Parent->Right)
	{
		if (Parent->Right->IsLeaf())
			RightPair = MinDistance_LeafLeaf(LeafOne, Parent->Right);
		else
			RightPair = MinDistance_LeafParent(LeafOne, Parent->Right);
	}

	if (LeftPair.Distance < 0)
		return RightPair;
	else if (RightPair.Distance < 0)
		return LeftPair;
	else if (RightPair.Distance < LeftPair.Distance)
		return RightPair;
	else
		return LeftPair;
}

FPartitionPair ADWorldGen::MinDistance_ParentParent(UPartitionNode* ParentOne, UPartitionNode* ParentTwo)
{
	FPartitionPair LeftPair, RightPair;
	LeftPair.Distance = -1.0f;
	RightPair.Distance = -1.0f;

	if (ParentOne->Left)
	{
		if (ParentOne->Left->IsLeaf())
			LeftPair = MinDistance_LeafParent(ParentOne->Left, ParentTwo);
		else
			LeftPair = MinDistance_ParentParent(ParentOne->Left, ParentTwo);
	}

	if (ParentOne->Right)
	{
		if (ParentOne->Right->IsLeaf())
			RightPair = MinDistance_LeafParent(ParentOne->Right, ParentTwo);
		else
			RightPair = MinDistance_ParentParent(ParentOne->Right, ParentTwo);
	}

	if (LeftPair.Distance < 0)
		return RightPair;
	else if (RightPair.Distance < 0)
		return LeftPair;
	else if (RightPair.Distance < LeftPair.Distance)
		return RightPair;
	else
		return LeftPair;
}

void ADWorldGen::GenMesh(FWorldGenDat& FloorWorldDat, FWorldGenDat& WallWorldDat, FWorldGenDat& CeilWorldDat)
{
	int32 XPos, YPos;

	for (XPos = 0; XPos < GridWidth; XPos++)
	{
		FString Chain = TEXT("");

		for (YPos = 0; YPos < GridHeight; YPos++)
		{
			if(IsRoomAt(XPos, YPos))
			{

				// Always generate a floor and ceiling for room tiles.
				GenFloor(XPos, YPos, FloorWorldDat);
				GenCeiling(XPos, YPos, CeilWorldDat);

				// Check for each type of wall
				if (HasWall(XPos, YPos, -1, 0))
					GenWall_Left(XPos, YPos, WallWorldDat);
				if (HasWall(XPos, YPos, 1, 0))
					GenWall_Right(XPos, YPos, WallWorldDat);
				if (HasWall(XPos, YPos, 0, 1))
					GenWall_Back(XPos, YPos, WallWorldDat);
				if (HasWall(XPos, YPos, 0, -1))
					GenWall_Front(XPos, YPos, WallWorldDat);
			}

		}

	}
}

void ADWorldGen::FinishGen(FWorldGenDat& FloorWorldDat, FWorldGenDat& WallWorldDat, FWorldGenDat& CeilWorldDat)
{
	// Add mesh sections and collision info.
	WorldMesh->CreateMeshSection_LinearColor(0, FloorWorldDat.Vertices, FloorWorldDat.Indices, FloorWorldDat.Normals, FloorWorldDat.TexCoords, FloorWorldDat.Colors, FloorWorldDat.Tangents, true);
	WorldMesh->CreateMeshSection_LinearColor(1, WallWorldDat.Vertices, WallWorldDat.Indices, WallWorldDat.Normals, WallWorldDat.TexCoords, WallWorldDat.Colors, WallWorldDat.Tangents, true);
	WorldMesh->CreateMeshSection_LinearColor(2, CeilWorldDat.Vertices, CeilWorldDat.Indices, CeilWorldDat.Normals, CeilWorldDat.TexCoords, CeilWorldDat.Colors, CeilWorldDat.Tangents, true);
	
	WorldMesh->SetMaterial(0, FloorMaterial);
	WorldMesh->SetMaterial(1, WallMaterial);
	WorldMesh->SetMaterial(2, CeilingMaterial);

	WorldMesh->ContainsPhysicsTriMeshData(true);
}

bool ADWorldGen::HasWall(int32 TileX, int32 TileY, int32 SideX, int32 SideY)
{
	int32 ActualX = TileX + SideX;
	int32 ActualY = TileY + SideY;

	if (IsRoomAt(TileX, TileY))
	{
		if (ActualX < 0 || ActualY >= GridWidth || ActualY < 0 || ActualY >= GridHeight)
		{
			return true;
		}
		else if (!IsRoomAt(ActualX, ActualY))
		{
			return true;
		}
	}

	// We don't consider empty rooms to be eligible for walls (this is a consistency thing as we don't want two walls to spawn in the same place for obvious reasons)
	return false;
}

bool ADWorldGen::IsRoomAt(int32 TileX, int32 TileY)
{
	if (TileX < 0 || TileX >= GridWidth || TileY < 0 || TileY >= GridHeight)
	{
		return false;
	}
	else if (IsRoom[TileY][TileX])
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ADWorldGen::GenFloor(int32 TileX, int32 TileY, FWorldGenDat& GenDat)
{
	FVector Bottom_BottomLeft = TileVertex_Bottom_BottomLeft(TileX, TileY);
	FVector Bottom_TopLeft = TileVertex_Bottom_TopLeft(TileX, TileY);
	FVector Bottom_TopRight = TileVertex_Bottom_TopRight(TileX, TileY);
	FVector Bottom_BottomRight = TileVertex_Bottom_BottomRight(TileX, TileY);

	GenDat.Vertices.Add(Bottom_BottomLeft);
	GenDat.Vertices.Add(Bottom_TopLeft);
	GenDat.Vertices.Add(Bottom_TopRight);
	GenDat.Vertices.Add(Bottom_BottomRight);

	// Two triangles of a square
	GenDat.Indices.Add(FloorVerts + 2);
	GenDat.Indices.Add(FloorVerts + 1);
	GenDat.Indices.Add(FloorVerts + 0);
	GenDat.Indices.Add(FloorVerts + 3);
	GenDat.Indices.Add(FloorVerts + 2);
	GenDat.Indices.Add(FloorVerts + 0);

	FloorVerts += 4;

	GenDat.TexCoords.Add(FVector2D(0, 0));
	GenDat.TexCoords.Add(FVector2D(0, 1));
	GenDat.TexCoords.Add(FVector2D(1, 1));
	GenDat.TexCoords.Add(FVector2D(1, 0));

	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);

	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
}

void ADWorldGen::GenWall_Front(int32 TileX, int32 TileY, FWorldGenDat& GenDat)
{
	FVector Bottom_BottomLeft = TileVertex_Bottom_BottomLeft(TileX, TileY);
	FVector Top_BottomLeft = TileVertex_Top_BottomLeft(TileX, TileY);
	FVector Top_TopLeft = TileVertex_Top_TopLeft(TileX, TileY);
	FVector Bottom_TopLeft = TileVertex_Bottom_TopLeft(TileX, TileY);

	GenDat.Vertices.Add(Bottom_BottomLeft);
	GenDat.Vertices.Add(Top_BottomLeft);
	GenDat.Vertices.Add(Top_TopLeft);
	GenDat.Vertices.Add(Bottom_TopLeft);

	// Two triangles of a square
	GenDat.Indices.Add(WallVerts + 2);
	GenDat.Indices.Add(WallVerts + 1);
	GenDat.Indices.Add(WallVerts + 0);
	GenDat.Indices.Add(WallVerts + 3);
	GenDat.Indices.Add(WallVerts + 2);
	GenDat.Indices.Add(WallVerts + 0);

	WallVerts += 4;

	GenDat.TexCoords.Add(FVector2D(0, 0));
	GenDat.TexCoords.Add(FVector2D(0, 1));
	GenDat.TexCoords.Add(FVector2D(1, 1));
	GenDat.TexCoords.Add(FVector2D(1, 0));

	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);

	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
}

void ADWorldGen::GenWall_Back(int32 TileX, int32 TileY, FWorldGenDat& GenDat)
{
	FVector Bottom_TopRight = TileVertex_Bottom_TopRight(TileX, TileY);
	FVector Top_TopRight = TileVertex_Top_TopRight(TileX, TileY);
	FVector Top_BottomRight = TileVertex_Top_BottomRight(TileX, TileY);
	FVector Bottom_BottomRight = TileVertex_Bottom_BottomRight(TileX, TileY);

	GenDat.Vertices.Add(Bottom_TopRight);
	GenDat.Vertices.Add(Top_TopRight);
	GenDat.Vertices.Add(Top_BottomRight);
	GenDat.Vertices.Add(Bottom_BottomRight);

	// Two triangles of a square
	GenDat.Indices.Add(WallVerts + 2);
	GenDat.Indices.Add(WallVerts + 1);
	GenDat.Indices.Add(WallVerts + 0);
	GenDat.Indices.Add(WallVerts + 3);
	GenDat.Indices.Add(WallVerts + 2);
	GenDat.Indices.Add(WallVerts + 0);

	WallVerts += 4;

	GenDat.TexCoords.Add(FVector2D(0, 0));
	GenDat.TexCoords.Add(FVector2D(0, 1));
	GenDat.TexCoords.Add(FVector2D(1, 1));
	GenDat.TexCoords.Add(FVector2D(1, 0));

	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);

	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
}

void ADWorldGen::GenWall_Right(int32 TileX, int32 TileY, FWorldGenDat& GenDat)
{
	FVector Bottom_TopLeft = TileVertex_Bottom_TopLeft(TileX, TileY);
	FVector Top_TopLeft = TileVertex_Top_TopLeft(TileX, TileY);
	FVector Top_TopRight = TileVertex_Top_TopRight(TileX, TileY);
	FVector Bottom_TopRight = TileVertex_Bottom_TopRight(TileX, TileY);

	GenDat.Vertices.Add(Bottom_TopLeft);
	GenDat.Vertices.Add(Top_TopLeft);
	GenDat.Vertices.Add(Top_TopRight);
	GenDat.Vertices.Add(Bottom_TopRight);

	// Two triangles of a square
	GenDat.Indices.Add(WallVerts + 2);
	GenDat.Indices.Add(WallVerts + 1);
	GenDat.Indices.Add(WallVerts + 0);
	GenDat.Indices.Add(WallVerts + 3);
	GenDat.Indices.Add(WallVerts + 2);
	GenDat.Indices.Add(WallVerts + 0);

	WallVerts += 4;

	GenDat.TexCoords.Add(FVector2D(0, 0));
	GenDat.TexCoords.Add(FVector2D(0, 1));
	GenDat.TexCoords.Add(FVector2D(1, 1));
	GenDat.TexCoords.Add(FVector2D(1, 0));

	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);

	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
}

void ADWorldGen::GenWall_Left(int32 TileX, int32 TileY, FWorldGenDat& GenDat)
{
	FVector Bottom_BottomRight = TileVertex_Bottom_BottomRight(TileX, TileY);
	FVector Top_BottomRight = TileVertex_Top_BottomRight(TileX, TileY);
	FVector Top_BottomLeft = TileVertex_Top_BottomLeft(TileX, TileY);
	FVector Bottom_BottomLeft = TileVertex_Bottom_BottomLeft(TileX, TileY);

	GenDat.Vertices.Add(Bottom_BottomRight);
	GenDat.Vertices.Add(Top_BottomRight);
	GenDat.Vertices.Add(Top_BottomLeft);
	GenDat.Vertices.Add(Bottom_BottomLeft);

	// Two triangles of a square
	GenDat.Indices.Add(WallVerts + 2);
	GenDat.Indices.Add(WallVerts + 1);
	GenDat.Indices.Add(WallVerts + 0);
	GenDat.Indices.Add(WallVerts + 3);
	GenDat.Indices.Add(WallVerts + 2);
	GenDat.Indices.Add(WallVerts + 0);

	WallVerts += 4;

	GenDat.TexCoords.Add(FVector2D(0, 0));
	GenDat.TexCoords.Add(FVector2D(0, 1));
	GenDat.TexCoords.Add(FVector2D(1, 1));
	GenDat.TexCoords.Add(FVector2D(1, 0));

	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);

	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
}

void ADWorldGen::GenCeiling(int32 TileX, int32 TileY, FWorldGenDat& GenDat)
{
	FVector Top_BottomLeft = TileVertex_Top_BottomLeft(TileX, TileY);
	FVector Top_TopLeft = TileVertex_Top_TopLeft(TileX, TileY);
	FVector Top_TopRight = TileVertex_Top_TopRight(TileX, TileY);
	FVector Top_BottomRight = TileVertex_Top_BottomRight(TileX, TileY);

	GenDat.Vertices.Add(Top_BottomLeft);
	GenDat.Vertices.Add(Top_TopLeft);
	GenDat.Vertices.Add(Top_TopRight);
	GenDat.Vertices.Add(Top_BottomRight);

	// Two triangles of a square
	GenDat.Indices.Add(CeilVerts + 0);
	GenDat.Indices.Add(CeilVerts + 1);
	GenDat.Indices.Add(CeilVerts + 2);
	GenDat.Indices.Add(CeilVerts + 0);
	GenDat.Indices.Add(CeilVerts + 2);
	GenDat.Indices.Add(CeilVerts + 3);

	CeilVerts += 4;

	GenDat.TexCoords.Add(FVector2D(0, 0));
	GenDat.TexCoords.Add(FVector2D(0, 1));
	GenDat.TexCoords.Add(FVector2D(1, 1));
	GenDat.TexCoords.Add(FVector2D(1, 0));

	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);
	GenDat.Colors.Add(FLinearColor::White);

	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
	GenDat.Normals.Add(FVector(1, 0, 0));
}

FVector ADWorldGen::TileVertex_Bottom_BottomLeft(int32 Row, int32 Col)
{
	return FVector(Row * TileSize, Col * TileSize, 0.0f);
}

FVector ADWorldGen::TileVertex_Bottom_BottomRight(int32 Row, int32 Col)
{
	return FVector(Row * TileSize, Col * TileSize + TileSize, 0.0f);
}

FVector ADWorldGen::TileVertex_Bottom_TopLeft(int32 Row, int32 Col)
{
	return FVector(Row * TileSize + TileSize, Col * TileSize, 0.0f);
}

FVector ADWorldGen::TileVertex_Bottom_TopRight(int32 Row, int32 Col)
{
	return FVector(Row * TileSize + TileSize, Col * TileSize + TileSize, 0.0f);
}

FVector ADWorldGen::TileVertex_Top_BottomLeft(int32 Row, int32 Col)
{
	return FVector(Row * TileSize, Col * TileSize, RoomHeight);
}

FVector ADWorldGen::TileVertex_Top_BottomRight(int32 Row, int32 Col)
{
	return FVector(Row * TileSize, Col * TileSize + TileSize, RoomHeight);
}

FVector ADWorldGen::TileVertex_Top_TopLeft(int32 Row, int32 Col)
{
	return FVector(Row * TileSize + TileSize, Col * TileSize, RoomHeight);
}

FVector ADWorldGen::TileVertex_Top_TopRight(int32 Row, int32 Col)
{
	return FVector(Row * TileSize + TileSize, Col * TileSize + TileSize, RoomHeight);
}
